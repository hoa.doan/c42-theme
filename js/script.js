$(document).ready(function () {

  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
  if(isIE) {
    $('body').addClass('ie');
  }
  if(isSafari) {
    $('body').addClass('safari');
  }
  // circle spin
  var radius = 170; // adjust to move out items in and out
  var fields = $('.item'),
    circle = $('#circle'),
    width = circle.width(),
    height = circle.height();
  var angle = 0,
    step = (2 * Math.PI) / fields.length;
  fields.each(function () {
    var x = Math.round(width / 2 + radius * Math.cos(angle) - $(this).width() / 2);
    var y = Math.round(height / 2 + radius * Math.sin(angle) - $(this).height() / 2);
    $(this).css({
      left: (x) + 'px',
      top: (y) + 'px'
    });
    angle += step;
  });

  $('.bar-menu').click(function () {
    $(this).toggleClass('open');
    $('.menu.mobile, html').toggleClass('open');
  });

  $('.popup-job img.close-term').click(function () {
    $('.popup-job').removeClass('show');
  });


  $('span.simulator:not(#button-header-form)').click(function (e) {
    if (!$(this).closest('form.free-download').hasClass('show')) {
      $(this).closest('form.free-download').addClass('show');
      $(this).closest('form.free-download input.submit').val('GET LINK');
      $(this).hide();
      $(this).closest('form.free-download').find('span.simulator-submit').css('display', 'inline-block');
      $(this).closest('form.free-download').find('input#mce-EMAIL').focus();
    }
  });
  $('span.simulator-submit').click(function(){
    $(this).closest('form.free-download').find('#mc-embedded-subscribe').trigger('submit')
  });


  var timeout = null;

  $('.div-normal input.type').on('keyup',function(){
    var email = $(this).val();

    if (timeout !== null) {
      clearTimeout(timeout);
    }
    var _this = this;
    timeout = setTimeout(function () {
      if(email != '') {
        if (validateEmail(email)) {
          $(_this).removeClass('mce_inline_error')
          $(_this).parent().find('.error p').text('');
          $(_this).parent().find('i.fa').show();
          $('#mce-error-response').text('');
        } else {
          $(_this).addClass('mce_inline_error')
          $(_this).next('i.fa').hide();
        }
      }else {
        $(_this).removeClass('mce_inline_error')
        $(_this).parent().find('i.fa').hide();
      }
    }, 500);
    return false;
  });

  $('form.free-download').submit(function(){
    var email = $(this).find('.div-normal input.type').val();
    var _this = this;
    //
    var capcha_position = $(this).find('.RecaptchaField').data('position');
    var response = grecaptcha.getResponse(capcha_position);

    if(response.length == 0) {
    // if(false) {
      $(_this).find('#mce-error-response').text('reCaptcha not verified');
    }
    else {
      $.ajax({
        type: 'POST',
        url: $(_this).attr('action'),
        data : $(_this).serialize(),
        cache       : false,
        dataType    : 'json',
        contentType: "application/json; charset=utf-8",
        error       : function(err) { alert("Could not connect to the registration server. Please try again later."); },
        success     : function(data) {
          if(data.result == 'success') {
            $(_this).addClass('success');
            $(_this).find('#mce-error-response').text('');
          }else {
            if(!validateEmail(email)) {
              $(_this).find('#mce-error-response').text('You inserted an incorrect email!');
              $(_this).find('input#mce-EMAIL').addClass('mce_inline_error')
            }else {
              if( $(_this).closest('.menu').length > 0 ) {
                alert(data.msg);
              }else {
                $(_this).find('#mce-error-response').html(data.msg);
                $(_this).removeClass('mce_inline_error')
                $(_this).parent().find('i.fa').hide();
              }
            }
          }
        }
      });
    }
  });

  $('.front').click(function(){
      $('.menu.desktop, header .logo').addClass('rotate');
  });
  $('.container').click(function(){
      $('.menu.desktop, header .logo').removeClass('rotate');
  })

  // fadeIn / fadeOut Page

  $("body").fadeIn(200);

  $("header li.menu-item > a, .logo a").click(function(event){
    event.preventDefault();
    linkLocation = this.href;
    $("body").fadeOut(200, redirectPage);
  });

  function redirectPage() {
    window.location = linkLocation;
  }

  // show again form mailchimp
  $('.div-success > input.submit').click(function(e){
    e.preventDefault();
    $(this).closest('form.free-download').removeClass('success');
    $(this).closest('form.free-download').find('#mce-EMAIL').val('');
    $(this).closest('form.free-download').find('.fa-check').hide();
  });

  //

  $('#header-form').on('click', function(e) {
    if (e.target !== this)
      return;

    $('html').removeClass('overlay');
  });

  $('.term-popup').each(function(){
    $(this).on('click', function(e) {
      if (e.target !== this)
        return;
      $(this).removeClass('show');
      $('.nav-footer li.menu-item').removeClass('active');
    });
  });

///////////////////////////////

  // Create Cookie
  $('.confirm').click(function(){
    setCookie( 'exoknoxCookie', 'true', '7' );
    $('.cookie').fadeOut();
  })
  //check exist cookie
  var Cookie = getCookie("exoknoxCookie");

  if (Cookie == "" || Cookie == null) {
    $('.cookie').show();
  }
  else {
    $('.cookie').hide();
  }

});
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

(function($) {
  $.fn.clickToggle = function(func1, func2) {
    var funcs = [func1, func2];
    this.data('toggleclicked', 0);
    this.click(function() {
      var data = $(this).data();
      var tc = data.toggleclicked;
      $.proxy(funcs[tc], this)();
      data.toggleclicked = (tc + 1) % 2;
    });
    return this;
  };
}(jQuery));


window.onload = init;
function init() {
  if (window.Event) {
    document.captureEvents(Event.MOUSEMOVE);
  }
  document.onmousemove = showCoords;
}

function showCoords(evt){
  var Y = evt.clientY
  if( Y < ( 10 + 150 )) {
    $('.menu.desktop, header .logo').addClass('rotate');
  }else {
    $('.menu.desktop, header .logo').removeClass('rotate');
  }
}

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}