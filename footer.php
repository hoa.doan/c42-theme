    <footer>
        <div class="nav-footer">
            <div class="text-left">
                <div class="logo">
                    <a href="<?php echo site_url() ?>/company">
                        <img src="<?php echo theme_uri() ?>/images/logo-footer.svg">
                    </a>
                </div>
                <div class="text">
                  <?php the_field('footer_text','option') ?>
                </div>
            </div>
            
            <ul class="nav-menu">
                <li class="menu-item"><a href="#" data-popup="legal">Legal</a></li>
                <li class="menu-item"><a href="#" data-popup="privacy">Privacy</a></li>
                <li class="menu-item"><a href="#" data-popup="provider">Provider</a></li>
            </ul>
        </div>
    </footer>
    <div class="cookie">
        <div class="text">
            <p>This website uses cookies to ensure you get the best experience on our website.</p>
        </div>
        <div class="confirm">
            <span>Got it!</span>
        </div>
    </div>
    <div class="term-popup" id="m-legal">
        <div class="wrap">
            <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">

                <h1>Legal</h1>

            <div class="text3">
                <?php the_field('legal','option') ?>
            </div>
        </div>
    </div>
    <div class="term-popup" id="m-privacy">
        <div class="wrap">
            <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">

                <h1>Privacy</h1>

            <div class="text3">
                <?php the_field('privacy','option') ?>
            </div>
        </div>
    </div>
    <div class="term-popup" id="m-provider">
        <div class="wrap">
            <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">
                <h1>Provider</h1>
            <div class="text3">
                <?php the_field('provider','option') ?>
            </div>
        </div>
    </div>
    <script>
      $(document).ready(function () {
          $('.nav-footer li.menu-item a').click(function(e){
              e.preventDefault();
              $('.term-popup').removeClass('show');
              var popup = $(this).data('popup');
              $('#m-'+popup).addClass('show');

            $(this).closest('li.menu-item').addClass('active');

            $('#m-'+popup+' .wrap').addClass('show-wrap');
            $('html').addClass('open');
          });

          $('.close-term').click(function(){
              $('.term-popup .wrap').removeClass('show-wrap');
            $('.nav-footer li.menu-item').removeClass('active');
            $('.term-popup').removeClass('show');

            $('html').removeClass('open');
          });
      })
    </script>
    <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
</div>
</body>
</html>