<div class="popup-job" id="legal">
  <div class="content">
    <div class="wrapper">
      <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">
      <div class="text1">
        <p>Legal</p>
      </div>
      <div class="text3">
        <?php the_field('legal','option') ?>
      </div>
    </div>
  </div>
</div>
<div class="popup-job" id="privacy">
  <div class="content">
    <div class="wrapper">
      <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">
      <div class="text1">
        <p>Privacy</p>
      </div>
      <div class="text3">
        <?php the_field('privacy','option') ?>
      </div>
    </div>
  </div>
</div>
<div class="popup-job" id="provider">
  <div class="content">
    <div class="wrapper">
      <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">
      <div class="text1">
        <p>Provider</p>
      </div>
      <div class="text3">
        <?php the_field('provider','option') ?>
      </div>
    </div>
  </div>
</div>