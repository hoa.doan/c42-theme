<?php get_header(); ?>
<style>
    html, body {
        height: 100%;
    }
    html body #wrapper {
        min-height: 100%;
        position: relative;
        height: auto;
        background-color: #F8F8F8;
    }
    .container, html body#jobs #wrapper .container .right .step1 {
        min-height: 100%;
        height: auto;
    }
</style>
<div class="container">
    <div class="content left">
        <div class="section">
            <div class="wrap">
                <div>
                    <div class="text">
                        <div class="text1">
                            <p>Jobs</p>
                        </div>
                        <div class="text2">
                           <ul class="jobs">
                             <?php
                             $i = 1;
                             if( have_rows('jobs', 'option') ):
                               
                               while ( have_rows('jobs','option') ) : the_row();
                                 
                                 ?>
                                   <li data-job="<?php echo $i ?>">
                                       <div class="item-body">
                                           <div>
                                               <div class="title">
                                                   <a href="#"><?php the_sub_field('job_title') ?></a>
                                               </div>
                                               <i class="fa fa-chevron-right"></i>
                                               <i class="fa fa-chevron-left"></i>
                                           </div>
                                       </div>
                                       <div class="bg"></div>
                                   </li>
                                 <?php
                                
                                 $i++;
                               endwhile;
                             endif;
  
                             ?>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="right">
        <div class="step step1 show">
            <div class="wrap">
                <div class="animation">
                    <div class="img-wrap">
                        <img src="<?php echo theme_uri() ?>/images/jobs.png" alt="">
                    </div>
                </div>
            </div>
            <div class="wrap-job">
          <?php
          $j = 1;
          if( have_rows('jobs', 'option') ):
  
          while ( have_rows('jobs','option') ) : the_row();
  
          ?>
            
                    <div class="popup-job" id="job<?php echo $j ?>">
                        <div class="content">
                            <div class="wrapper">
                                <div class="logojob">
                                    <img src="<?php echo get_template_directory_uri()?>/images/Complex42_big.svg" alt="">
                                </div>
                                <div class="text1">
                                    <p><?php the_sub_field('job_headline') ?></p>
                                </div>
                                <div class="text2">
                                    <div>
                                        <span class="label">Location</span>
                                        <span class="value"><?php the_sub_field('job_location') ?></span>
                                    </div>
                                    <div>
                                        <span class="label">Starting date</span>
                                        <span class="value"><?php the_sub_field('job_date') ?></span>
                                    </div>
                                </div>
                                <div class="text3">
                                  <?php the_sub_field('job_content') ?>
                                </div>
                                <div class="link-download apply">
                                    <a href="mailto:info@complex42.com?subject=<?php the_sub_field('job_headline') ?>">APPLY</a>
                                </div>
                            </div>
                        </div>
                    </div>
            
            <?php
            $j++;
          endwhile;
          endif;
          ?>
            </div>
        </div>
    </div>
</div>
<div class="container mobile">
    <div class="content left">
        <div class="section">
            <div class="wrap">
                <div>
                    <div class="image step1">
                        <div class="img-wrap">
                            <img src="<?php echo theme_uri() ?>/images/jobs.png" alt="">
                        </div>
                    </div>
                    <div class="text">
                        <div class="text1">
                            <p>Jobs</p>
                        </div>
                        <div class="text2">
                            <ul class="jobs">
                              <?php
                              $i = 1;
                              if( have_rows('jobs', 'option') ):
                                
                                while ( have_rows('jobs','option') ) : the_row();
                                  
                                  ?>
                                    <li data-job="<?php echo $i ?>">
                                        <div class="item-body">
                                            <div>
                                                <div class="title">
                                                    <a href="#"><?php the_sub_field('job_title') ?></a>
                                                </div>
                                                <i class="fa fa-chevron-right"></i>
                                                <i class="fa fa-chevron-left"></i>
                                            </div>
                                        </div>
                                        <div class="bg"></div>
                                    </li>
                                  <?php
                                  
                                  $i++;
                                endwhile;
                              endif;
                              
                              ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wrap-job">
  <?php
  $j = 1;
  if( have_rows('jobs', 'option') ):
    
    while ( have_rows('jobs','option') ) : the_row();
      
      ?>
        <div class="popup-job" id="job<?php echo $j ?>">
            <div class="content">
                <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">
                <div class="logojob">
                    <img src="<?php echo get_template_directory_uri()?>/images/Complex42_big.svg" alt="">
                </div>
                <div class="text1">
                    <p><?php the_sub_field('job_headline') ?></p>
                </div>
                <div class="text2">
                    <div>
                        <span class="label">Location</span>
                        <span class="value">Berlin</span>
                    </div>
                    <div>
                        <span class="label"><?php the_sub_field('job_location') ?></span>
                        <span class="value"><?php the_sub_field('job_date') ?></span>
                    </div>
                </div>
                <div class="text3">
                  <?php the_sub_field('job_content') ?>
                </div>
                <div class="link-download apply">
                    <a href="mailto:info@complex42.com?subject=<?php the_sub_field('job_headline') ?>">APPLY</a>
                </div>
            </div>
        </div>
      <?php
      $j++;
    endwhile;
  endif;
  ?>
    </div>
</div>
<script>
  
  $('ul.jobs > li').click(function(){
    var data = $(this).data('job');
    if($(this).hasClass('active')) {
        $(this).removeClass('active')
      $('div#job' + data).removeClass('show');
      $('html').removeClass('open');
    }else {
      $('ul.jobs > li').removeClass('active');
      $(this).toggleClass('active');

      $('.popup-job').removeClass('show');
      $('div#job' + data).addClass('show');
      
      $('html').addClass('open');
    }
  });

  $('.close-term').click(function(){
    $('ul.jobs > li').removeClass('active');
    $('html').removeClass('open');
  });
</script>
<?php get_footer();?>
