<?php get_header(); ?>



<style>
    html, body {
        height: 100%;
    }
    html body #wrapper {
        min-height: 100%;
        position: relative;
        height: auto;
        background-color: #F8F8F8;
    }
</style>

<script src="<?php echo theme_uri() ?>/animation/anim-data/orb-loop-intro.json"></script>

<div class="container">
  <div class="content left">
    <div class="section">
      <div class="wrap">
        <div>
          <?php
          if( get_field('company_logo', 'option') != null ) {
            ?>
              <div class="logo">
                  <img src="<?php the_field('company_logo','option') ?>" alt="">
              </div>
            <?php
          }
          ?>
          <div class="text">
            <?php
            if( get_field('complex_headline', 'option') != null ) {
              ?>
                <div class="text1">
                  <?php the_field('complex_headline','option') ?>
                </div>
              <?php
            }
         
            if( get_field('complex_text', 'option') != null ) {
              ?>
                <div class="text2">
                  <?php the_field('complex_text','option') ?>
                </div>
              <?php
            }
            if( get_field('team_headline', 'option') != null ) {
              ?>
                <div class="text1">
                  <?php the_field('team_headline','option') ?>
                </div>
              <?php
            }
            if( get_field('team_text', 'option') != null ) {
              ?>
                <div class="text2">
                  <?php the_field('team_text','option') ?>
                </div>
              <?php
            }
            ?>
          </div>
          <div class="link-download">
            <a href="<?php echo site_url() ?>/jobs">JOIN</a>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--  <div class="right">
    <div class="step step1 show">
      <div class="animation">
          <div class="img-wrap">
              <div id="intro-orb-container-company" class="animation-panel"></div>
             &lt;!&ndash; <img src="<?php echo theme_uri() ?>/images/company.png" alt="">&ndash;&gt;
          </div>
      </div>
    </div>
  </div>-->

    <div class="right">
        <div id="intro-orb-container2" class="animation-panel"></div>
    </div>


</div>
<div class="container mobile">
    <div class="content left">
        <div class="section">
            <div class="wrap">
                <div>
                    <div class="logo">
                        <img src="<?php echo theme_uri() ?>/images/company-logo.png" alt="">
                    </div>
                    <div class="text">
                      <?php
                      if( get_field('complex_headline', 'option') != null ) {
                        ?>
                          <div class="text1">
                            <?php the_field('complex_headline','option') ?>
                          </div>
                        <?php
                      }
                      
                      if( get_field('complex_text', 'option') != null ) {
                        ?>
                          <div class="text2">
                            <?php the_field('complex_text','option') ?>
                          </div>
                        <?php
                      }
                      if( get_field('team_headline', 'option') != null ) {
                        ?>
                          <div class="text1">
                            <?php the_field('team_headline','option') ?>
                          </div>
                        <?php
                      }
                      if( get_field('team_text', 'option') != null ) {
                        ?>
                          <div class="text2">
                            <?php the_field('team_text','option') ?>
                          </div>
                        <?php
                      }
                      ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo theme_uri() ?>/animation/js/lottie.js"></script>

<script>

    var introOrbLoopCompany = {
        wrapper: document.getElementById('intro-orb-container2'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: '/c42/wp-content/themes/exoknox/animation/anim-data/orb-loop-intro.json'
    };

    introAnim = lottie.loadAnimation(introOrbLoopCompany);

    introAnim.addEventListener('DOMLoaded', function () {
        introAnim.play();
        introAnim.loop = true;
    });

</script>

<?php get_footer();?>
