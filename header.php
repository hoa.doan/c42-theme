<!DOCTYPE html>
<html>
<head>
    <title>Exoknox</title>
    <link rel="shortcut icon" href="<?php echo theme_uri() ?>/images/fa-icon.png" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    
    <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="all">
    <link rel="stylesheet"  href="<?php echo theme_uri().'/css/reset.css'  ?>" type="text/css" media="all">
    <link rel="stylesheet"  href="<?php echo theme_uri().'/css/style.css'  ?>" type="text/css" media="all">
    <link rel="stylesheet"  href="<?php echo theme_uri().'/css/itd-fix.css'  ?>" type="text/css" media="all">
    <link rel="stylesheet"  href="<?php echo theme_uri().'/css/mobile.css'  ?>" type="text/css" media="all">



    <script src="<?php echo theme_uri()?>/js/jquery-3.2.1.min.js"></script>
    <script src="<?php echo theme_uri()?>/js/jquery.hover3d.js"></script>
    <script src="<?php echo theme_uri()?>/js/script.js"></script>


    <script type="text/javascript">
        var theme_uri = '<?php echo theme_uri() ?>';
      var CaptchaCallback = function() {
        grecaptcha.render('RecaptchaField1', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField2', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField3', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField4', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField5', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField6', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField7', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
        grecaptcha.render('RecaptchaField8', {'sitekey' : '6LdN_kwUAAAAAL6v_kZAdn5JkoaG5VZF4493SRSp', 'theme' : 'light'});
      };
    </script>
    <?php wp_head(); ?>
</head>
<body <?php itd_body_id() ?> <?php body_class() ?> >
<?php

global $iPhone;
global $iPad;

global $mc_u;
global $mc_id;

?>
<div id="header-form">
    <div class="wrap">
        <div>
            <img src="<?php echo get_template_directory_uri()?>/images/close.svg" alt="" class="close-term">
            <div class="text1">
                <p>Download Exoknox</p>
            </div>
            <div class="text2">
                <p>Please insert your email and we'11 send <br>
                    you the download link by email. <br>
                    If you didn't get the email right away <br>
                    please check if it went to Spam.
                </p>
            </div>
            <div class="download">
              <?php echo form_mailchimp($mc_u, $mc_id, '', true,'1') ?>
            </div>
        </div>
    </div>
</div>
<div id="wrapper">
    <header>
        <div class="logo">
            <div class="before">
                <a href="<?php echo site_url() ?>">
                    <img src="<?php echo theme_uri() ?>/images/logo-header.svg" alt="">
                </a>
            </div>
           <div class="after">
               <a href="<?php echo site_url() ?>">
                   <img src="<?php echo theme_uri() ?>/images/logo-header.svg" alt="">
               </a>
           </div>
        </div>
        <div class="bar-menu">
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div class="menu desktop">
            <div class="flip">
                <span class="front">
                    <i class="fa fa-angle-up"></i>
                </span>
                <ul class="back">
                  <?php
                  wp_nav_menu(array(
                    'theme_location' => 'main_menu',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'fallback_cb' => 'alert_menu'
                  ));
                  ?>
                    <li>
                        <div class="download">
                            <form action="" disabled>
                                <span class="button submit simulator" id="button-header-form">
                                <span>FREE DOWNLOAD</span>
                             </span>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="menu mobile">
            <ul>
              <?php
              wp_nav_menu(array(
                'theme_location' => 'main_menu',
                'container' => false,
                'items_wrap' => '%3$s',
                'fallback_cb' => 'alert_menu'
              ));
              ?>
            </ul>
        </div>
    </header>
    <script>
        $('#button-header-form').click(function (e) {
            e.preventDefault();
            $('html').addClass('overlay');
        });
        $('#header-form .close-term').click(function () {
          $('html').removeClass('overlay');
        });
    </script>