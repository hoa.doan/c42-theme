<?php get_header(); ?>
<style>
    html, body {
       height: 100%;
    }
    html body #wrapper {
        min-height: 100%;
        position: relative;
        height: auto;
        background-color: #F8F8F8;
    }
  .container, html body#contact #wrapper .container .right .step1 {
    height: calc(100% - 71px);
  }
</style>
<div class="container">
  <div class="content left">
    <div class="section">
      <div class="wrap">
        <div>
          <div class="text">
            <div class="text1">
              <p>Get in touch</p>
            </div>
            <div class="text2">
              <?php
              if( get_field('email', 'option') != null ) {
                ?>
                  <p><span class="label">Email:</span>
                      <span class="value"><a href="mailto:<?php the_field('email','option') ?>"><?php the_field('email','option') ?></a></span></p>
                <?php
              } ?>
  
              <?php
              if( get_field('phone', 'option') != null ) {
                ?>
                  <p><span class="label">Phone:</span>
                      <span class="value"><a href="tel:<?php the_field('phone','option') ?>"><?php the_field('phone','option') ?></a></span></p>
                <?php
              } ?>
         
            </div>
          </div>
          <div class="link-download">
            <a href="mailto:<?php the_field('email','option') ?>">SEND EMAIL</a>
          </div>
              <div class="small-text">
                <?php
                if( get_field('small_headline', 'option') != null ) {
                ?>
                  <div class="text1">
                      <p><?php the_field('small_headline', 'option') ?></p>
                  </div>
                  <?php
                } ?>
                
                
                <?php
                if( get_field('small_text', 'option') != null ) {
                ?>
                  <div class="text2">
                    <?php the_field('small_text', 'option') ?>
                  </div>
                  <?php
                } ?>
              </div>
         
        </div>
      </div>
    </div>
  </div>
  <div class="right">
    <div class="step step1 show">
      <div class="animation">
          <div class="img-wrap">
              <img src="<?php echo theme_uri() ?>/images/contact.png" alt="">
          </div>
      </div>
    </div>
  </div>
</div>
<div class="container mobile">
    <div class="content left">
        <div class="section">
            <div class="wrap">
                <div>
                    <div class="image">
                        <img src="<?php echo theme_uri() ?>/images/step4-animation.png" alt="">
                    </div>
                    <div class="text">
                        <div class="text1">
                            <p>Get in touch</p>
                        </div>
                        <div class="text2">
                          <?php
                          if( get_field('email', 'option') != null ) {
                            ?>
                              <p><span class="label">Email:</span>
                                  <span class="value"><a href="mailto:<?php the_field('email','option') ?>"><?php the_field('email','option') ?></a></span></p>
                            <?php
                          } ?>
                          
                          <?php
                          if( get_field('phone', 'option') != null ) {
                            ?>
                              <p><span class="label">Phone:</span>
                                  <span class="value"><a href="tel:<?php the_field('phone','option') ?>"><?php the_field('phone','option') ?></a></span></p>
                            <?php
                          } ?>

                        </div>
                    </div>
                    <div class="link-download">
                        <a href="mailto:<?php the_field('email','option') ?>">SEND EMAIL</a>
                    </div>
                    <div class="small-text">
                      <?php
                      if( get_field('small_headline', 'option') != null ) {
                        ?>
                          <div class="text1">
                              <p><?php the_field('small_headline', 'option') ?></p>
                          </div>
                        <?php
                      } ?>
                      
                      
                      <?php
                      if( get_field('small_text', 'option') != null ) {
                        ?>
                          <div class="text2">
                            <?php the_field('small_text', 'option') ?>
                          </div>
                        <?php
                      } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer();?>
