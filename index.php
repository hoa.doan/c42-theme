<?php get_header(); ?>


<script src="<?php echo theme_uri() ?>/animation/anim-data/orb-loop-intro.json"></script>
<script src="<?php echo theme_uri() ?>/animation/anim-data/main-animation.json"></script>
<script src="<?php echo theme_uri() ?>/animation/anim-data/end-loop-spin.json"></script>
<link rel="stylesheet"  href="<?php echo theme_uri().'/animation/css/main.css'  ?>" type="text/css" media="all">



<div id="contentid" class="container">
    <div id="all-content-wrapper" class="content left">


        <div id="content-box-1">
            <div class="section" data-step="1">
                <div class="wrap">
                    <div>
                        <?php
                        if( get_field('home_logo', 'option') != null ) {
                            ?>
                        <div class="logo">
                            <img src="<?php the_field('home_logo','option') ?>" alt="">
                        </div>
                        <?php
                        }
                    ?>
                        <div class="text">
                            <?php
                            if( get_field('step1_headline', 'option') != null ) {
                                ?>
                            <div class="text1">
                                <?php the_field('step1_headline','option') ?>
                            </div>
                            <?php
                            }
                        ?>
                            <?php
                      if( get_field('step1_text', 'option') != null ) {
                            ?>
                            <div class="text2">
                                <?php the_field('step1_text','option') ?>
                            </div>
                            <?php
                      }
                      ?>

                        </div>

                        <div class="download">
                            <?php echo form_mailchimp($mc_u, $mc_id, sanitize_text_field(get_field('step1_headline','option')), false, '2') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="content-box-2">
            <div class="section" >
                <div class="wrap">
                    <div>
                        <div class="text">
                            <?php
                          if( get_field('step2_headline', 'option') != null ) {
                            ?>
                            <div class="text1">
                                <?php the_field('step2_headline','option') ?>
                            </div>
                            <?php
                          }
                          ?>

                            <?php
                          if( get_field('step2_text', 'option') != null ) {
                            ?>
                            <div class="text2">
                                <?php the_field('step2_text','option') ?>
                            </div>
                            <?php
                          }
                          ?>
                        </div>
                        <div class="download">
                            <?php echo form_mailchimp($mc_u, $mc_id, sanitize_text_field(get_field('step2_headline','option')), false, '3') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="block-mid"></div>

        <div id="content-box-3">
            <div class="section " data-step="3">
                <div class="wrap">
                    <div>
                        <div class="text">
                            <?php
                      if( get_field('step3_headline', 'option') != null ) {
                        ?>
                            <div class="text1">
                                <?php the_field('step3_headline','option') ?>
                            </div>
                            <?php
                      }
                      ?>
                            <?php
                      if( get_field('step3_text', 'option') != null ) {
                        ?>
                            <div class="text2">
                                <?php the_field('step3_text','option') ?>
                            </div>
                            <?php
                      }
                      ?>
                        </div>
                        <div class="download">
                            <?php echo form_mailchimp($mc_u, $mc_id, sanitize_text_field(get_field('step3_headline','option')), false, '4') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
    <div class="right">
        <div id="intro-orb-container" class="animation-panel"></div>
        <div id="main-orb-container" class="animation-panel"></div>
        <div id="end-orb-container" class="animation-panel"></div>
    </div>
</div>

<div class="container mobile">
    <div class="content left">
        <div class="section">
            <div class="wrap">
                <div>
                    <?php
                  if( get_field('home_logo', 'option') != null ) {
                    ?>
                    <div class="logo">
                        <img src="<?php the_field('home_logo','option') ?>" alt="">
                    </div>
                    <?php
                  }
                  ?>
                    <div class="download first">
                        <?php echo form_mailchimp($mc_u, $mc_id, '', false, '5') ?>
                    </div>
                    <div class="image step1">
                        <div class="img-wrap">
                            <img src="<?php echo theme_uri() ?>/images/step1.png" alt="">
                        </div>
                    </div>
                    <div class="text">
                        <?php
                      if( get_field('step1_headline', 'option') != null ) {
                        ?>
                        <div class="text1">
                            <?php the_field('step1_headline','option') ?>
                        </div>
                        <?php
                      }
                      ?>
                        <?php
                      if( get_field('step1_text', 'option') != null ) {
                        ?>
                        <div class="text2">
                            <?php the_field('step1_text','option') ?>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="download">
                        <?php echo form_mailchimp($mc_u, $mc_id, sanitize_text_field(get_field('step1_headline','option')), false, '6') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="wrap">
                <div>
                    <div class="image step2">
                        <div class="img-wrap">
                            <img src="<?php echo theme_uri() ?>/images/step2.png" alt="">
                        </div>
                    </div>
                    <div class="text">
                        <?php
                      if( get_field('step2_headline', 'option') != null ) {
                        ?>
                        <div class="text1">
                            <?php the_field('step2_headline','option') ?>
                        </div>
                        <?php
                      }
                      ?>

                        <?php
                      if( get_field('step2_text', 'option') != null ) {
                        ?>
                        <div class="text2">
                            <?php the_field('step2_text','option') ?>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="download">
                        <?php echo form_mailchimp($mc_u, $mc_id, sanitize_text_field(get_field('step2_headline','option')), false, '7') ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="wrap">
                <div>
                    <div class="image step3">
                        <div class="img-wrap">
                            <img src="<?php echo theme_uri() ?>/images/step3.png" alt="">
                        </div>
                    </div>
                    <div class="text">
                        <?php
                      if( get_field('step3_headline', 'option') != null ) {
                        ?>
                        <div class="text1">
                            <?php the_field('step3_headline','option') ?>
                        </div>
                        <?php
                      }
                      ?>
                        <?php
                      if( get_field('step3_text', 'option') != null ) {
                        ?>
                        <div class="text2">
                            <?php the_field('step3_text','option') ?>
                        </div>
                        <?php
                      }
                      ?>
                    </div>
                    <div class="download">
                        <?php echo form_mailchimp($mc_u, $mc_id, sanitize_text_field(get_field('step3_headline','option')), false, '8') ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo theme_uri() ?>/animation/js/lottie.js"></script>
<script src="<?php echo theme_uri() ?>/animation/js/main.js"></script>


<script>
    // scroll
    //  $(window).scroll(function () {
    //    var element1Height = $('.section:first').height();
    //    var element2Height = $('.section:nth-child(2)').height();
    //    var element3Height = $('.section:nth-child(3)').height();
    //
    //    var element1Top = $('.section:first').offset().top;
    //    var element2Top = $('.section:nth-child(2)').offset().top;
    //    var element3Top = $('.section:nth-child(3)').offset().top;
    //
    //    var element1Bottom = element1Top + element1Height;
    //    var element2Bottom = element2Top + element2Height;
    //    var element3Bottom = element3Top + element3Height;
    //    var scrollTop = $(window).scrollTop();
    //    if(scrollTop > element1Top && scrollTop < (element1Bottom -  element1Height/ 1.5)) {
    //      $('.step').removeClass('show');
    //      $('.step1').addClass('show');
    //    }else if( scrollTop > (element2Top - element2Height / 1.5 )  && scrollTop < ( element2Bottom - element2Height / 1.5) ) {
    //      $('.step').removeClass('show');
    //      $('.step2').addClass('show');
    //    }else if( scrollTop > (element3Top - element3Height / 1.5 ) && scrollTop < ( element3Bottom - element3Height / 1.5) ) {
    //      $('.step').removeClass('show');
    //      $('.step3').addClass('show');
    //    }
    //  });
</script>


<?php get_footer();?>
