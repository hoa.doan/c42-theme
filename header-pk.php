<!DOCTYPE html>
<html>
<head>
  <title>Exoknox</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  
  
  <link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="all">
  <link rel="stylesheet"  href="<?php echo theme_uri().'/css/reset.css'  ?>" type="text/css" media="all">
  <link rel="stylesheet"  href="<?php echo theme_uri().'/css/style.css'  ?>" type="text/css" media="all">
  <link rel="stylesheet"  href="<?php echo theme_uri().'/css/mobile.css'  ?>" type="text/css" media="all">
  
  
  <script src="<?php echo theme_uri()?>/js/jquery-3.2.1.min.js"></script>
  <script src="<?php echo theme_uri()?>/js/jquery.hover3d.js"></script>
  <script src="<?php echo theme_uri()?>/js/script.js"></script>
  <?php wp_head(); ?>
</head>
<body <?php itd_body_id() ?> <?php body_class() ?> >
<?php

global $iPhone;
global $iPad;

global $mc_u;
global $mc_id;

?>
<div id="wrapper">
  <header>
    <div class="logo">
      <h1><a href="<?php echo site_url() ?>">EXOKNOX</a></h1>
    </div>
    <div class="bar-menu">
      <div></div>
      <div></div>
      <div></div>
    </div>
    <div class="menu desktop">
      <div class="flip">
                <span class="front">
                    <i class="fa fa-angle-up"></i>
                </span>
        <ul class="back">
          <?php
          wp_nav_menu(array(
            'theme_location' => 'main_menu',
            'container' => false,
            'items_wrap' => '%3$s',
            'fallback_cb' => 'alert_menu'
          ));
          ?>
          <li>
            <div class="download">
              <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
              <form class="free-download" action="https://test.us17.list-manage.com/subscribe/post-json?c=?" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div class="div-success">
                  <input readonly value="Success!" type="email" class="type">
                  <input disabled type="button" value="NEUTRAL" class="button submit">
                </div>
                <div class="div-normal">
                  <input type="hidden" name="u" value="<?php echo $mc_u ?>">
                  <input type="hidden" name="id" value="<?php echo $mc_id ?>">
                  <input type="hidden" value="<?php echo $headline ?>" name="SUBJECT" class="" id="mce-SUBJECT">
                  <input placeholder="Insert your email" type="email" name="EMAIL" class="required email type" id="mce-EMAIL">
                  <i class="fa fa-check"></i>
                  <input type="submit" value="GET LINK" name="subscribe" class="button submit" id="mc-embedded-subscribe">
                  <!--<input type="button" value="FREE DOWNLOAD" name="subscribe" class="button submit" id="simulator">-->
                  <div class="button-submit">
                    FREE DOWNLOAD
                    <div class="layer">FREE DOWNLOAD</div>
                  </div>
                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response"></div>
                  </div>
                </div>
              </form>
              <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <div class="menu mobile">
      <ul>
        <?php
        wp_nav_menu(array(
          'theme_location' => 'main_menu',
          'container' => false,
          'items_wrap' => '%3$s',
          'fallback_cb' => 'alert_menu'
        ));
        ?>
      </ul>
    </div>
  </header>
  <script>
    $('.menu .download').hover(function(){
      $(this).addClass('hover');
    },function(){
      $(this).removeClass('hover');
    });

    $('.menu .download .button-submit').click(function(){
      if (!$(this).closest('form.free-download').hasClass('show')) {
        $(this).closest('form.free-download').addClass('show');
        $(this).closest('form.free-download input.submit').val('GET LINK');
        $(this).hide();
        $(this).closest('form.free-download').find('input#mc-embedded-subscribe').show();
      }
    });
  
  </script>