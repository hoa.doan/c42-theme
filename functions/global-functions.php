<?php

function get_ID_by_slug( $page_slug ) {
    $page = get_page_by_path( $page_slug );
    if ( $page ) {
        return $page->ID;
    } else {
        return null;
    }
}


function theme_uri() {
    return get_template_directory_uri();
}


function echoif( $condition, $true, $false = '' ) {
    echo $condition ? $true : $false;
}


function itd_body_id() {
    global $post;
    $body_id = '';

    if ( is_front_page() ) {
        $body_id = 'home';

    } else if ( is_page() || is_single() ) {
        $body_id = $post->post_name;
    }

    echo "id='{$body_id}'";
}

function itd_log( $message ) {
    $file = WP_CONTENT_DIR . '/uploads/log.txt';

    $fh = fopen( $file, 'w+' );
    fwrite( $fh, date( MessageLogHelper::$date_format ) . ': ' . $message . PHP_EOL );
}

function itd_clear_log() {
    $file = WP_CONTENT_DIR . '/uploads/log.txt';
    unlink( $file );
}