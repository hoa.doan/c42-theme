<?php
/**
 * Created by PhpStorm.
 * User: Tri
 * Date: 9/7/2017
 * Time: 3:22 PM
 */

add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point($path) {
    $path = get_stylesheet_directory() . '/itd-acf-config';
    return $path;
}

add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point($paths) {
    unset($paths[0]);
    $paths[] = get_stylesheet_directory() . '/itd-acf-config';
    return $paths;
}

acf_add_options_page(array(
    'page_title'  => 'Home Page',
    'menu_title'  => 'Home Page',
    'menu_slug'  => 'home-page-options',
    'icon_url' => 'dashicons-admin-home'
));

acf_add_options_page(array(
  'page_title'  => 'Company Page',
  'menu_title'  => 'Company Page',
  'menu_slug'  => 'company-page-options',
  'icon_url' => 'dashicons-admin-home'
));

acf_add_options_page(array(
  'page_title'  => 'Jobs Page',
  'menu_title'  => 'Jobs Page',
  'menu_slug'  => 'jobs-page-options',
  'icon_url' => 'dashicons-admin-home'
));

acf_add_options_page(array(
  'page_title'  => 'Contact Page',
  'menu_title'  => 'Contact Page',
  'menu_slug'  => 'contact-page-options',
  'icon_url' => 'dashicons-admin-home'
));

acf_add_options_page(array(
  'page_title'  => 'Footer Content',
  'menu_title'  => 'Footer Content',
  'menu_slug'  => 'footer-content-options',
  'icon_url' => 'dashicons-admin-home'
));