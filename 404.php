<?php get_header()?>
    <style>
        html {
            height: 100%;
        }
    </style>
    <div class="container">
        <div class="content left">
            <div class="section">
                <div class="wrap">
                    <div>
                        <div class="text">
                            <div class="text1">
                              <p>Oops! <br>
                                  We can't seem to find the page you're looking for.
                              </p>
                            </div>
                            <div class="text2">
                                <p>Error code: 404 <br>
                                    <br>
                                    The link you clicked may be broken or the page may have been removed.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer()?>