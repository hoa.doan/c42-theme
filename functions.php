<?php
include "functions/acf-option-page.php";
include "functions/global-functions.php";

add_theme_support( 'post-thumbnails' );

function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

//Detect special conditions devices
$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
  remove_action('wp_head', '_admin_bar_bump_cb');
}


register_nav_menus(array(
  'main_menu' => 'Main Menu',
  'footer_menu' => 'Footer Menu',
));


//add SVG to allowed file uploads
function add_file_types_to_uploads($file_types){
  $new_filetypes = array();
  $new_filetypes['svg'] = 'image/svg+xml';
  $file_types = array_merge($file_types, $new_filetypes );
  return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

// SEO
function ttg_insert_fb_in_head() {
  global $post;
  if ( !is_singular()) //if it is not a post or a page
    return;
  echo '<meta property="og:title" content="' . get_the_title() . '"/>';
  echo '<meta property="og:type" content="article"/>';
  echo '<meta property="og:url" content="' . get_permalink() . '"/>';
  echo '<meta property="og:site_name" content="The Texture Group"/>';
  if(!has_post_thumbnail( $post->ID )) { //the post does not have featured image, use a default image
    $default_image="http://example.com/image.jpg"; //replace this with a default image on your server or an image in your media library
    echo '<meta property="og:image" content="' . $default_image . '"/>';
  }
  else{
    $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'medium' );
    echo '<meta property="og:image" content="' . esc_attr( $thumbnail_src[0] ) . '"/>';
  }
  echo "
";
}
add_action( 'wp_head', 'ttg_insert_fb_in_head', 5 );



function getUserIP()
{
  $client  = @$_SERVER['HTTP_CLIENT_IP'];
  $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
  $remote  = $_SERVER['REMOTE_ADDR'];
  
  if(filter_var($client, FILTER_VALIDATE_IP))
  {
    $ip = $client;
  }
  elseif(filter_var($forward, FILTER_VALIDATE_IP))
  {
    $ip = $forward;
  }
  else
  {
    $ip = $remote;
  }
  
  return $ip;
}

$user_ip = getUserIP();

$res = file_get_contents('https://www.iplocate.io/api/lookup/'.$user_ip);
$res = json_decode($res);

$country_code = $res->country_code;
$mc_u = '69a685ccc0a12302d2d220d27'; //exoknox
// $mc_u = 'cd1f05382781bf8b99dc045d0';
$uc_id = '';
if( $country_code == 'US' || $country_code == 'CA' ) {
// if( $country_code == 'DE' ) {
  $mc_id = 'b234babb37'; //exoknox
  // $mc_id = '520bc20eea';
}else {
  $mc_id = '72b854c327'; //exoknox
  // $mc_id = 'eb62f9607c';
}



function form_mailchimp($mc_u, $mc_id, $headline , $show, $capchaID) {
  $mc_url = 'https://test.us17.list-manage.com/subscribe/post-json?c=?'; // exoknox mailchimp
  // $mc_url = 'https://youknowmewell.us12.list-manage.com/subscribe/post-json?c=?'; // test
ob_start(); ?>
  <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
  <form class="free-download <?php echo $show ? 'show' : '' ?>" action="<?php echo $mc_url ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div class="div-success">
      <input readonly value="Success!" type="email" class="type">
      <input type="button" value="THANK YOU" class="button submit">
    </div>
    <div class="div-normal">
      <input type="hidden" name="u" value="<?php echo $mc_u ?>">
      <input type="hidden" name="id" value="<?php echo $mc_id ?>">
      <input type="hidden" value="<?php echo $headline ?>" name="SUBJECT" class="" id="mce-SUBJECT">
      <input placeholder="Insert your email" type="email" name="EMAIL" class="required email type" id="mce-EMAIL">
        
        <i class="fa fa-check"></i>
        <input type="submit" value="GET LINK" name="subscribe" class="button submit" id="mc-embedded-subscribe">
        <span class="button submit simulator-submit">
            <span>GET LINK</span>
        </span>
        <span class="button submit simulator">
            <span>FREE DOWNLOAD</span>
        </span>
    </div>
      <div class="div-normal mc-error">
          <div id="mce-responses" class="clear">
              <div class="response" id="mce-error-response"></div>
          </div>
      </div>
      <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
      <?php if($capchaID != '') {
          $position = $capchaID - 1;
          ?>
      <div class="RecaptchaField" id="RecaptchaField<?php echo $capchaID ?>" data-position="<?php echo $position ?>"></div>
    <?php } ?>
  </form>
  <?php return ob_get_clean();

}