<?php get_header(); ?>
<style>
    html, body {
        height: 100%;
    }
</style>
<div class="container">
    <div class="download">
        <form action="" class="free-download">
            <input type="text" placeholder="Insert your email" class="type">
            <input type="submit" value="FREE DOWNLOAD" class="submit">
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        $('.type').on('keyup',function(){
            var email = $(this).val();
            if (validateEmail(email)) {
                $(this).parent().find('.checked').show();
                $(this).css('border-color','#d6d6d6');
                $(this).parent().find('.error p').text('');
            } else {
                $(this).parent().find('.checked').hide();
                $(this).css('border-color','#D8786E');
                $(this).parent().find('.error p').text('You inserted an incorrect email!');
            }
            return false;
        })
        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

      $('input.submit').click(function(e){
        e.preventDefault();
        $('form.free-download').addClass('show');
        $('form.free-download input.submit').val('GET LINK');
      });
      
      $('body').click(function(e) {
        var target = $(e.target);
        if(!target.is('form.free-download input')) {
            $('form.free-download').removeClass('show')
            // $('form.free-download input.type').val('');
            $('form.free-download input.submit').val('FREE DOWNLOAD');
        }
      });
    })
</script>
<?php get_footer();?>
